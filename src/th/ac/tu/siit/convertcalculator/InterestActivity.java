package th.ac.tu.siit.convertcalculator;

import java.util.Locale;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class InterestActivity extends Activity implements OnClickListener {

	float InterestRate;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_interest);
		
		Button b1 = (Button)findViewById(R.id.calculate);
		b1.setOnClickListener(this);
		
		Button b2 = (Button)findViewById(R.id.itrsetting);
		b2.setOnClickListener(this);
		
		TextView intr = (TextView)findViewById(R.id.interestrate);
		InterestRate = Float.parseFloat(intr.getText().toString());
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.interest, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		int id = v.getId();
		
		if(id == R.id.calculate){
			EditText dollar = (EditText)findViewById(R.id.dollar);
			EditText year = (EditText)findViewById(R.id.year);
			TextView result = (TextView)findViewById(R.id.textView4);
			
			String res = "";
			try {
				float d = Float.parseFloat(dollar.getText().toString());
				float y = Float.parseFloat(year.getText().toString());
				double r = d*(Math.pow((1.0+InterestRate/100.0), y));
				res = String.format(Locale.getDefault(), "%.2f", r);
			} catch(NumberFormatException e) {
				res = "Invalid input";
			} catch(NullPointerException e) {
				res = "Invalid input";
			}
			result.setText(res);
		}else if(id == R.id.itrsetting){
			Intent i = new Intent(this, SettingActivity.class);
			i.putExtra("InterestRate", InterestRate);
			startActivityForResult(i, 9999);
		}
	}
	
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == 9999 && resultCode == RESULT_OK) {
			InterestRate = data.getFloatExtra("InterestRate", 10.0f);
			TextView itrRate = (TextView)findViewById(R.id.interestrate);
			itrRate.setText(String.format(Locale.getDefault(), "%.2f", InterestRate));
		}
	}


}
